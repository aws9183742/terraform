
variable "region" {}
variable "stack_prefix" {}
variable "stack_name" {}
#variable "kms_key" {}
variable "purpose"{}
variable "log_bucket_name"{}
variable "versioning_configuration" {}
variable "access_control" {}
variable "ownership_controls" {}
variable "s3_bucket_tags" {
  type    = map(string)
  default = {}
}
variable "transition" {
  type = list(object({
    StorageClass = string
    TransitionInDays = number
  }))
    default = []
}