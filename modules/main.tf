

data "aws_caller_identity" "current" {}
data "aws_region" "current" {}


resource "aws_cloudformation_stack" "s3_bucket_stack" {
  name ="stack-${var.stack_name}" 
  capabilities = ["CAPABILITY_AUTO_EXPAND"]
  template_body = jsonencode({
    AWSTemplateFormatVersion = "2010-09-09",
    Description = "CloudFormation stack for an S3 bucket with a lifecycle policy",
    Resources = {
      S3Bucket = {
        Type = "AWS::S3::Bucket",
        Properties = {
          BucketName = "ala-a${data.aws_caller_identity.current.account_id}-${var.purpose}-${data.aws_region.current.name}",
          AccessControl = var.access_control,
          VersioningConfiguration = {
            Status = var.versioning_configuration
          },
          
          OwnershipControls = {
            Rules = [
              {
                ObjectOwnership = var.ownership_controls
              }
            ]
          },
          "LifecycleConfiguration": {
            "Rules": [
              {
                "Id": "multiple rules",
                "Status": "Enabled",
                "Transitions": var.transition
              }
            ]
          },
          LoggingConfiguration = {
            DestinationBucketName = var.log_bucket_name,
            LogFilePrefix = "aws/s3access/ala-a${data.aws_caller_identity.current.account_id}-${var.purpose}-${data.aws_region.current.name}"
          },
          Tags = [
            for key, value in var.s3_bucket_tags : {
              Key   = key
              Value = value
            }
          ],
        }
      }
    },
    Outputs = {
      S3BucketName = {
        Description = "The name of the S3 bucket",
        Value = { "Ref" = "S3Bucket" }
      }
    }
})
}






